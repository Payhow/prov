let veri = {
    //位运算获取网络号函数封装
    catchNum:function(ip, sonip){
        let arr_sonip = sonip.split(".");
        let arr_ip = ip.split(".");
        let sequence = null;
        let res = [];

        for (let i = 0; i < arr_sonip.length; i++) {
            if (arr_sonip[i] == 0) {
                sequence = i;
                //说明此位以前都是要判断网段一不一样的
                for (let j = 0; j < i; j++) {
                    res.push(parseInt(arr_ip[j]) & parseInt(arr_sonip[j]));
                    // console.log(res);

                }
                break;
            }
        }
        return {res,sequence}
    },
    //池地址开始的网络号判断
    veriRelation: function (ip, sonip, start, rule) {

        let arr_start = start.split(".");
        let sequence = null;
        let res = [];

        let obj = veri.catchNum(ip,sonip);
        sequence = obj.sequence;
        res = obj.res;
        arr_start.splice(sequence);
        if (res.join(".") == arr_start.join(".")) {
            return true;
        }
        else {
            rule.innerHTML = '非法IP地址。根据IP地址和子网掩码，该IP溢出DHCP分配范围。';
            return false;
        }
    },
    //池地址结束的网络号判断
    veriRelation2: function (ip, sonip, end, rule) {

        let arr_end = end.split(".");
        let sequence = null;
        let res = [];

        let obj = veri.catchNum(ip,sonip);
        sequence = obj.sequence;
        res = obj.res;
        arr_end.splice(sequence);
        if (res.join(".") == arr_end.join(".")) {
            return true;
        }
        else {
            rule.innerHTML = '非法IP地址。根据IP地址和子网掩码，该IP溢出DHCP分配范围。';
            return false;
        }
    },
    veriCompare: function (sonip, start, end, rule) {
        let arr_sonip = sonip.split(".");
        let arr_start = start.split(".");
        let arr_end = end.split(".");
        for (let i = 0; i < arr_sonip.length; i++) {
            if (parseInt(arr_sonip[i]) !== 255) {
                for (let j = i; j < arr_start.length; j++) {
                    if (parseInt(arr_start[j]) > parseInt(arr_end[j])) {
                        rule.innerHTML = '池地址开始地址不能大于池地址结束地址';
                        return false;
                    }
                }
            }
        }
    },
    //主机位不能全为0或1的判断
    veriMainNum: function (ip, sonip, rule) {
        let arr_ip = ip.split(".");
        let res = [];

        let obj = veri.catchNum(ip,sonip);
        res = obj.res;
        res.forEach((item)=>{
            let index = arr_ip.indexOf(item.toString());
            // console.log("看下相等不",item.toString()===arr_ip[0] );
            // console.log("看下index",index);
            if(index!==-1){
                arr_ip.splice(index,1);
            }
        });
        // console.log("显示iparr",arr_ip);
        let every1 = arr_ip.every((item)=>{
            return item == 0;
        });
        let every2 = arr_ip.every((item)=>{
            return item == 255;
        });
        if(every1||every2){
            rule.innerHTML = '主机位不能全为0或1;IP地址错误或者子网掩码错误!';
            return false;
        }
        else{
            return true;
        }
    },
    getNetNum: function (sonip, start, end) {
        let arr_sonip = sonip.split(".");
        let arr_start = start.split(".");
        let arr_guestStart = [];
        let arr_end = end.split(".");
        let arr_guestEnd = [];
        arr_guestStart = JSON.parse(JSON.stringify(arr_start));
        arr_guestEnd = JSON.parse(JSON.stringify(arr_end));
        for (let i = 0; i < arr_sonip.length; i++) {
            if (parseInt(arr_sonip[i]) !== 255) {
                arr_guestStart[i - 1] = parseInt(arr_start[i - 1]) + 1;
                arr_guestEnd[i - 1] = parseInt(arr_end[i - 1]) + 1;
                break;
            }
        }
        return [arr_guestStart.join("."), arr_guestEnd.join(".")];
    },
    //ip校验
    veriIp: function (val,mask, rule) {
        let arr_ip = val.split(".");
        let reg = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        if (!reg.test(val)) {
            rule.innerHTML = '非法IP格式，请输入1-223开头的某个有效IP地址。';
            return false;
        }
        else{
            if (arr_ip.length !== 0) {
                if (arr_ip[0] > 223 || arr_ip[0] < 1) {
                    rule.innerHTML = '非法IP格式，请输入1-223开头的某个有效IP地址。';
                    return false;
                }
                if(val=="1.0.0.0"){
                    return false;
                }
                else return true;
            }
            return true;
        }
    },
    //掩码校验
    maskVali(val, rule) {
        let reg = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        rule.innerHTML = '非法掩码格式';
        if (!reg.test(val)) {
            return false;
        }
        var buff = [];
        val.split(/\./g).forEach(function (byte) {
            buff.push(parseInt(byte, 10) & 0xff)
        })
        console.log(buff);
        for (let k in buff) {
            if (buff[k] !== 0 && buff[k] !== 128 && buff[k] !== 192 && buff[k] !== 224 && buff[k] !== 240 && buff[k] !== 248 && buff[k] !== 252 && buff[k] !== 254 && buff[k] !== 255) {
                return false;
            }
        }

        let m = [],
            mn = val.split('.');

        if (val == '0.0.0.0' || val == '128.0.0.0' || val == '192.0.0.0' || val == '224.0.0.0' || val == '240.0.0.0' || val == '248.0.0.0' || val == '252.0.0.0' || val == '254.0.0.0') {
            rule.innerHTML = '掩码不在合法范围内';
            return false;
        }
        if (val == '255.255.255.255') {
            return true;
        }
        if (mn.length == 4) {
            for (let i = 0; i < 4; i++) {
                m[i] = mn[i];
            }
        } else {
            return false;
        }

        let v = (m[0] << 24) | (m[1] << 16) | (m[2] << 8) | (m[3]);
        //将掩码进行二进制与运输，得到整串子网掩码的十进制
        let f = 0;
        for (let k = 0; k < 32; k++) {
            if ((v >> k) & 1) {
                f = 1;
            } else if (f == 1) {
                return false;
            }
        }
        if (f == 0) {
            return false;
        }
        for (let i = 0; i < 4; i++) {
            let t = /^\d{1,}$/;
            //校验掩码是数字
            if (!t.test(mn[i])) {
                return false;
            }
        }
        return true;
    },
}
export default veri;
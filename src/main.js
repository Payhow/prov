import Vue from 'vue'
import App from './App.vue'
import veri from './util/ipVeri.js'
import {
  Switch,
  Input,
  // Form,
  // FormItem,
  Button,
  MessageBox,
  Message,
} from 'element-ui'

Vue.use(Switch);
Vue.use(Input);
// Vue.use(Form);
// Vue.use(FormItem);
Vue.use(Button);
// Vue.use(MessageBox);
// Vue.use(Message);
Vue.component(MessageBox.name,MessageBox);
Vue.component(Message.name,Message);

Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$message = Message;

Vue.prototype.$veri = veri;
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
